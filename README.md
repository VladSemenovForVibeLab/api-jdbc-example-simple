# README для приложения на Java 17 с использованием Maven, Spring Boot и JDBC

Это пример приложения на Java 17, которое использовует JDBC API для создания и просмотра одной модели данных с идентификатором, именем и полем "tech". Приложение использует Maven для управления зависимостями и Spring Boot для создания веб-приложения.

## Требования
- Java 17
- Maven
- H2 (или другая база данных, совместимая с JDBC)

## Установка

1. Клонируйте репозиторий на свой компьютер:

```
git clone git@gitlab.com:VladSemenovForVibeLab/api-jdbc-example-simple.git
```

2. Создайте базу данных MySQL с именем "database_name" или измените настройки базы данных в файле `src/main/resources/application.properties`:

```
spring.datasource.url=jdbc:mysql://localhost:3306/database_name
spring.datasource.username=root
spring.datasource.password=
```

3. Запустите приложение с помощью Maven:

```
cd SpringJDBC
mvn spring-boot:run
```

## Использование

1. Добавьте новую запись, введя имя и значение поля "tech" в текстовые поля и нажав кнопку "Добавить".

2. Просмотрите все записи. Отобразятся все добавленные записи с их идентификаторами, именами и значениями поля "tech".

## Примечания

- В данном примере используется встроенная база данных H2. Однако, вы можете изменить настройки базы данных в файле `src/main/resources/application.properties` для использования MySQL или другой совместимой базы данных.