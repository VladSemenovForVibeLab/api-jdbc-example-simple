package ru.svf.SpringJDBC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import ru.svf.SpringJDBC.model.Alien;
import ru.svf.SpringJDBC.repository.AlienRepository;

import java.util.List;

@SpringBootApplication
public class SpringJdbcApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(SpringJdbcApplication.class, args);
		Alien alien = context.getBean(Alien.class);
		alien.setId(1);
		alien.setName("Alien1");
		alien.setTech("Java");
		AlienRepository alienRepository = context.getBean(AlienRepository.class);
		alienRepository.save(alien);
		List<Alien> all = alienRepository.findAll();
		System.out.println(all);
	}
}
