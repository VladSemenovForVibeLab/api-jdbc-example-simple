package ru.svf.SpringJDBC.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.svf.SpringJDBC.model.Alien;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class AlienRepository {
    private final JdbcTemplate jdbcTemplate;
    public void save(Alien alien) {
        String sql = "INSERT INTO alien (id,name, tech) VALUES (?,?,?)";
        jdbcTemplate.update(sql, alien.getId(), alien.getName(), alien.getTech());
        System.out.println(sql);
    }
    public List<Alien> findAll() {
        String sql = "SELECT * FROM alien";
        RowMapper<Alien> rowMapper = new RowMapper<Alien>() {
            @Override
            public Alien mapRow(ResultSet rs, int rowNum) throws SQLException {
                Alien alien = new Alien();
                alien.setId(rs.getInt("id"));
                alien.setName(rs.getString("name"));
                alien.setTech(rs.getString("tech"));
                return alien;
            }
        };
        List<Alien> query = jdbcTemplate.query(sql, rowMapper);
        return query;
    }
}
